const { json } = require("express/lib/response");
const request = require("supertest");
const app = require("./app");

const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IndpbGxpYW0iLCJlbWFpbCI6IndpbGxpYW1AZ21haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjEsIm5hbWUiOiJDVVNUT01FUiJ9LCJpYXQiOjE2NTQ3ODQ5NTN9.O9u9mqOpk0ix2Wfon62SQyFpJuBCSxaVguSB9PcZW1U";

const cust =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IndpbGxpYW0iLCJlbWFpbCI6IndpbGxpYW1AZ21haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjEsIm5hbWUiOiJDVVNUT01FUiJ9LCJpYXQiOjE2NTQ3ODQ5NTN9.O9u9mqOpk0ix2Wfon62SQyFpJuBCSxaVguSB9PcZW1U";

describe("landing", () => {
  it("landing /", () => {
    request(app)
      .get("/")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.objectContaining({
            status: "OK",
            message: "BCR API is up and running!",
          })
        );
      });
  });
});

describe("Auth", function () {
  it("register", () => {
    request(app)
      .post("/v1/auth/register")
      .send({
        name: "william",
        email: "william@gmail.com",
        password: "william20",
      })
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        expect(response.body).toEqual(
          expect.objectContaining({
            accessToken: expect.any(String),
          })
        );
      });
  });

  it("post login -> success", () => {
    request(app)
      .post("/v1/auth/login")
      .send({
        email: "william@gmail.com",
        password: "wiliam123",
      })
      .expect("Content-Type", /json/)
      .expect(201);
  });

  it("post login -> wrongp pass", () => {
    request(app)
      .post("/v1/auth/login")
      .send({
        email: "william@gmail.com",
        password: "wiliam123",
      })
      .expect("Content-Type", /json/)
      .expect(401);
  });

  it("post login -> email not registerd", () => {
    request(app)
      .post("/v1/auth/login")
      .send({
        email: "william@gmail.com",
        password: "wiliam123",
      })
      .expect("Content-Type", /json/)
      .expect(404);
  });

  it("post auth whoami -> success", () => {
    request(app)
      .post("/v1/auth/whoami")
      .set("Authorization", `Bearer ${token}`)
      .expect("Content-Type", /json/)
      .expect(200);
  });

  it("post auth whoami -> 401", () => {
    request(app)
      .post("/v1/auth/whoami")
      .expect("Content-Type", /json/)
      .expect(401);
  });
});

describe("API", () => {
  it("get cars -> success", async () => {
    return request(app)
      .get("/v1/cars")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        expect(res.body).toEqual(
          expect.objectContaining({
            cars: expect.any(Array),
            meta: expect.any(Object),
          })
        );
      });
  });

  it("get cars -> not found", () => {
    request(app).get("/v1/cars").expect("Content-Type", /json/).expect(404);
  });

  it("get cars/id -> success", () => {
    request(app)
      .get("/v1/cars/1")
      .set("Authorization", `Bearer ${token}`)
      .expect(200)
      .then((response) => {
        response.body.toEqual(
          expect.objectContaining({
            createdAt: "2022-06-08T02L29L12.007Z",
            id: 1,
            image: "https://source.unsplash.com/500x500",
            isCurrentlyRented: false,
            name: "Toyota Corolla",
            price: 300000,
            size: "SMALL",
            updateAt: "2022-06-08T02:29:12:006Z",
          })
        );
      });
  });

  describe("GET ERROR 404", () => {
    it("GET /todos/id --> 404 if not found", () => {
      return request(app).get("/v1/carss").expect(404);
    });
  });
});
